package test;

/**
 * Ejercicio "Complementing a DNA Strand"
 * 
 * Crear proyecto de tipo Java version 1.8
 * 
 * 
 * 
 * 
 * @author cristiandonoso
 *
 */
public class Merc {

	public static void main(String[] args) {
		//Cadena de ADN a procesar.
		String cadenaAdn = "ACCGGGTTTT";
		//Respuesta
		System.out.println(getCadenaAdn(cadenaAdn));
	}
	
	/**
	 *  Metodo que recibe un String como parametro, en este caso una "secuencia de ADN" a evaluar, segun el ejemplo.
	 * este devuelve una respuesta de tipo String que contiene dicha secuencia de ADN, 
	 * invierte dicha secuencia y obtiene cada complemento segun la cadena String de la secuencia invertida.
	 * 
	 * @param cadenaDna String
	 * @return cadenaAdn
	 */
	public static String getCadenaAdn(String cadenaAdn){
		 String response = "";
		 String cadenaInvertida = invertirCadena(cadenaAdn);
	        String [] adnArray = cadenaInvertida.split("");
	        for(String caracterAEvaluar : adnArray){
	        	response += getComplementoAdn(caracterAEvaluar);
	            }
	        return response;
	}
	
	/**
	 *  Metodo que retorna el complemento que existe entre cada base nitrogenada que contiene el adn
	 * Ejemplo : 
	 * 
	 *  	> adenina → A es complemento de T
	 * 		> timina → T es complemento de A
	 * 		> citosina → C es complemento de G
	 * 		> guanina → G es complemento de C
	 *  
	 *  cada base nitrogenada esta representada por un caracter especifico como se muestra en el ejemplo y retorna dicho complemento
	 * @param caracter String
	 * @return
	 */
	private static String getComplementoAdn(String caracterAEvaluar){
        String caracter = "";
        switch (caracterAEvaluar){
            case "A": caracter = "T";
                    break;
            case "T": caracter = "A";
                    break;
            case "G": caracter = "C";
                    break;
            case "C": caracter = "G";
                    break;
        }
        return caracter;
    }
	
	/**
	 *  Metodo que retorna una secuencia de tipo String invertida.
	 * @param cadenaAInvertir String
	 * @return stringIvertido String
	 */
	public static String invertirCadena(String cadenaAInvertir) {
		return new StringBuilder(cadenaAInvertir).reverse().toString();
	}

}
